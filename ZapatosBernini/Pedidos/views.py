from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from Pedidos.forms import SignUpForm
from django.template.context_processors import request
from django.urls import reverse_lazy
from django.db import transaction
from django.views.generic import CreateView, UpdateView, DeleteView, ListView
from django.contrib.auth.models import User
from .models import Pedido, Articulo, PedidoLineas
from .forms import PedidoLineasFormSet, PedidoLineasForm
from .serializers import ArticuloSerializer
from rest_framework import generics
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from io import StringIO
import csv

@login_required
def home(request):
    return redirect('pedido-list')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            nombre = form.cleaned_data.get('nombre')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=nombre, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


class PedidoList(ListView):
    model = Pedido

    def get_queryset(self):
        return Pedido.objects.filter(cliente=self.request.user)


class PedidoCreate(CreateView):
    model = Pedido
    fields = ['cliente','direccion','codigo_postal','ciudad','provincia','pais']
    

class PedidoPedidoLineasCreate(CreateView):
    model = Pedido
    fields = ['cliente','direccion','codigo_postal','ciudad','provincia','pais']
    success_url = reverse_lazy('pedido-list')

    def get_context_data(self, **kwargs):
        data = super(PedidoPedidoLineasCreate, self).get_context_data(**kwargs) 
        if self.request.POST:
            data['pedidolineas'] = PedidoLineasFormSet(self.request.POST)
        else:
            data['pedidolineas'] = PedidoLineasFormSet()
        usuario=self.request.user
        data['form'].fields['cliente'].queryset = User.objects.filter(username=usuario)
        data['form'].fields['cliente'].initial=usuario
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        pedidolineas = context['pedidolineas']
        with transaction.atomic():
            self.object = form.save()

            if pedidolineas.is_valid():
                pedidolineas.instance = self.object
                pedidolineas.save()
        return super(PedidoPedidoLineasCreate, self).form_valid(form)
 
 
class PedidoPedidoLineasUpdate(UpdateView):
    model = Pedido
    fields = ['cliente','direccion','codigo_postal','ciudad','provincia','pais']
    success_url = reverse_lazy('pedido-list')

    def get_context_data(self, **kwargs):
        data = super(PedidoPedidoLineasUpdate, self).get_context_data(**kwargs)
        if self.request.POST:
            data['pedidolineas'] = PedidoLineasFormSet(self.request.POST, instance=self.object)
        else:
            data['pedidolineas'] = PedidoLineasFormSet(instance=self.object)
        usuario=self.request.user
        data['form'].fields['cliente'].queryset = User.objects.filter(username=usuario)
        data['form'].fields['cliente'].initial=usuario
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        pedidolineas = context['pedidolineas']
        with transaction.atomic():
            self.object = form.save()

            if pedidolineas.is_valid():
                pedidolineas.instance = self.object
                pedidolineas.save()
        return super(PedidoPedidoLineasUpdate, self).form_valid(form)


class PedidoDelete(DeleteView):
    model = Pedido
    success_url = reverse_lazy('pedido-list')

class ArticuloRestList(generics.ListCreateAPIView):
   queryset = Articulo.objects.all()
   serializer_class = ArticuloSerializer
    
class ArticuloRestUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = Articulo.objects.all()
    serializer_class = ArticuloSerializer
    
def PedidoDetailhtml(request,pk):
    pedido = Pedido.objects.get(id=pk)
    queryset = PedidoLineasFormSet(instance=pedido).queryset
    context = {
        'pedido': pedido,
        'queryset': queryset,
        }
    return render (request, 'Pedidos/pedido_detailhtml.html', context)
    
def PedidoSendmail(request, pk):
    htmly     = get_template('Pedidos/pedido_detailhtml.html')
    pedido = Pedido.objects.get(id=pk)
    queryset = PedidoLineasFormSet(instance=pedido).queryset
    context = {
        'pedido': pedido,
        'queryset': queryset,
        }
    html_content = htmly.render(context)
    
    csvfile = StringIO()
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(['Pedido_id', 'Cliente', 'Fecha', 'Direccion', 'Codigo Postal', 'Ciudad', 'Provincia', 'Pais', 'Total pedido'])
    csvwriter.writerow([pedido.id, pedido.cliente, pedido.fecha, pedido.direccion, pedido.codigo_postal, pedido.ciudad, pedido.provincia, pedido.pais, pedido.total_pedido])
    csvwriter.writerow([])
    csvwriter.writerow(['articulo', 'unidades', 'subtotal' ])
    for linea in queryset:
        csvwriter.writerow([linea.articulo, linea.unidades, linea.subtotal ])
    
    subject  = 'Pedido ' + pk + ' de Zapatos Bernini'
    from_email = 'n2mofu.svr@gmail.com' 
    to = ['n2mofu.svr@gmail.com',]
    text_content = 'Pedido procesado por e-mail'
    msg = EmailMultiAlternatives(subject, text_content, from_email, to)
    msg.attach_alternative(html_content, "text/html")
    msg.attach('pedido.csv', csvfile.getvalue(), 'text/csv')
    msg.send()
    
    pedido.delete()
    
    return render (request, 'Pedidos/pedido_sendmail.html')