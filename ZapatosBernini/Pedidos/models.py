# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Avg, Count, Min, Sum
from django.contrib import admin
from django.contrib.auth.models import User
from enum import unique

# Create your models here.

class Articulo(models.Model):
    referencia = models.CharField('Referencia',
                                  help_text='Referencia que identifica al artículo',
                                  max_length=6,
                                  unique=True
                                 )
    descripcion = models.CharField('Descripción',
                                   help_text='Descripción del artículo',
                                   max_length=100)
    precio = models.FloatField('Precio',
                                 help_text='Precio del artículo')
    def __str__(self):
        return "{0} - {1}".format(self.referencia,self.descripcion)


class Pedido(models.Model):
    cliente = models.ForeignKey(User,
                                #editable=False,
                                on_delete=models.CASCADE)
    fecha = models.DateField('Fecha del Pedido',
                             help_text='Fecha del pedido',
                             auto_now_add=True)
    total_pedido  = models.FloatField('Total',
                                       help_text='Total del pedido',
                                       null=True,
                                       editable=False
                                       )
    direccion = models.CharField('Direccion de Entrega',
                                 help_text='Direccion de entrega del pedido',
                                 max_length=200)
    codigo_postal = models.IntegerField('Código Postal',
                                        help_text='Código postal de entrega del pedido')
    ciudad = models.CharField('Ciudad',
                              help_text='Ciudad de entrega del pedido',
                              max_length=100) 
    provincia = models.CharField('Provincia',
                              help_text='Provincia de entrega del pedido',
                              max_length=100) 
    pais = models.CharField('País',
                              help_text='País de entrega del pedido',
                              max_length=100)
    def __str__(self):
        return "Pedido número {0}".format(self.id)
    
    def save(self, *args, **kwargs):
        a = Pedido.objects.filter(id=self.id).aggregate(total=Sum('pedidolineas__subtotal'))
        self.total_pedido = a['total']
        super(Pedido, self).save(*args, **kwargs)
        
    
class PedidoLineas(models.Model):
    pedido = models.ForeignKey(Pedido,
                               on_delete=models.CASCADE)
    articulo = models.ForeignKey(Articulo,
                                 on_delete=models.CASCADE)
    unidades = models.IntegerField('Unidades',
                                   help_text='Unidades solicitadas')
    subtotal = models.FloatField('Subtotal',
                                   help_text='Subtotal de la linea',
                                   null=True,
                                   editable=False)
    def __str__(self):
        return "Línea {0} del pedido número {1}".format(self.id,self.pedido.id)
    
    def save(self, *args, **kwargs):
        self.subtotal = self.unidades * self.articulo.precio        
        super(PedidoLineas, self).save(*args, **kwargs)


class Pedido_Lineas_Inline(admin.TabularInline):
    model = PedidoLineas


class PedidoAdmin(admin.ModelAdmin):
    inlines = (Pedido_Lineas_Inline,)
    
admin.site.register(Pedido,PedidoAdmin)
admin.site.register(Articulo)
    