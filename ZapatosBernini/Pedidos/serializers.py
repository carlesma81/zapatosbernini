from rest_framework import serializers
from Pedidos.models import Articulo
from dataclasses import field

class ArticuloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articulo
        fields = ('id','referencia','descripcion','precio')