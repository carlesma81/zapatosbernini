from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm, inlineformset_factory
from .models import Pedido, PedidoLineas

class SignUpForm(UserCreationForm):
    nombre = forms.CharField(max_length=30,help_text='Requerido. 30 carácteres como máximo')
    apellidos = forms.CharField(max_length=100,help_text='Requerido. 100 carácteres como máximo')
    email = forms.EmailField(max_length=100,help_text='Requerido. 100 carácteres como máximo')

    class Meta:
        model = User
        fields = ('username', 'nombre', 'apellidos', 'email', 'password1', 'password2', )


class PedidoForm(ModelForm):
    class Meta:
        model = Pedido
        exclude = ()
        

class PedidoLineasForm(ModelForm):
    class Meta:
        model = PedidoLineas
        exclude = ()
        
        
PedidoLineasFormSet = inlineformset_factory(Pedido, PedidoLineas,form=PedidoLineasForm, extra=10)
