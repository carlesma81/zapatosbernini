from django.conf.urls import url

from Pedidos import views
from django.contrib.auth.decorators import login_required
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    url(r'^pedido/$', login_required(views.PedidoList.as_view()), name='pedido-list'),
    url(r'^pedido/add/$', login_required(views.PedidoPedidoLineasCreate.as_view()), name='pedido-add'),
    url(r'^pedido/(?P<pk>[0-9]+)/$', login_required(views.PedidoPedidoLineasUpdate.as_view()), name='pedido-update'),
    url(r'^pedido/(?P<pk>[0-9]+)/delete/$', login_required(views.PedidoDelete.as_view()), name='pedido-delete'),
    url(r'pedido/view/(?P<pk>[0-9]+)/$', views.PedidoDetailhtml, name='pedido-detailhtml'),
    url(r'pedido/sendmail/(?P<pk>[0-9]+)/$', views.PedidoSendmail, name='pedido-sendmail'),
    url(r'^articulo/$',views.ArticuloRestList.as_view()),
    url(r'^articulo/(?P<pk>[0-9]+)/$',views.ArticuloRestUpdate.as_view()),
    url(r'^documentacion/$',schema_view),
]
