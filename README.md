Instrucciones:

1.- Instalar virtualenv:
(según S.O.)

2.-Crear entorno virtual para python 3.7.4+:
virtualenv venv -p python3

3.- Instalar las dependencias de software detalladas en el fichero ZapatosBernini/requirements.txt
pip install -r requirements.txt

4.- Arrancar servidor de test en localhost
python manage.py runserver 8000

5.- Probar la aplicación:
http://localhost:8000


Notas:

-Para poder crear pedidos nos requerirá crear una cuenta de usuario e iniciar sessión con ella (también se puede usar admin/1234 que ya tiene pedidos creados 
y es la cuenta de administrador).

-En http://localhost:8000/articulo/ tenemos el endpoint con la api para el mantenimiento de los articulos y precios.

-En http://localhost:8000/documentacion/ podremos ver la documentación del endpoint creada mediante swagger.

-Cuando envienmos un correo con el pedido este se eliminrá del listado de pedidos y llegará, con su csv adjunto, 
a la cuentra de correo creada para la ocasión: n2mofu.svr@gmail.com

-El password de la cuenta de correo es: P4ssw0rd@SVR